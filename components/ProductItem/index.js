import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Icon } from 'semantic-ui-react';

const ProductItem = ({ name }) => (
  <Card>
    <Image src="https://react.semantic-ui.com/assets/images/avatar/large/matthew.png" />
    <Card.Content>
      <Card.Header>
        {name}
      </Card.Header>
      <Card.Meta>
        <span className="date">
          Joined in 2015
        </span>
      </Card.Meta>
      <Card.Description>
        Matthew is a musician living in Nashville.
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <a>
        <Icon name="user" />
        22 Friends
      </a>
    </Card.Content>
  </Card>
);

ProductItem.propTypes = {
  name: PropTypes.string.isRequired,
};

export default ProductItem;

