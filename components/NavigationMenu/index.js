import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Container, Menu, Image, Button } from 'semantic-ui-react';
import Link from './link';

const NavigationMenu = ({ transparentHeader }) => (
  <div className={transparentHeader && 'navigationContainer'}>
    <style jsx>
      {`
        .navigationContainer {
          position: absolute !important;
          width: 100%;
          z-index: 2000;
        }
      `}
    </style>
    <Segment basic>
      <Container>
        <Menu secondary borderless>
          <Menu.Item fitted="horizontally">
            <Image src="/static/images/logo.png" height="40" />
          </Menu.Item>
          <Link name="home" href="/" />
          <Link name="shop" href="/shop" />
          <Menu.Menu position="right">
            <Menu.Item fitted="horizontally">
              <Button fluid>
                Log In
              </Button>
            </Menu.Item>
            <Menu.Item fitted="horizontally">
              <Button fluid>
                Sign Up
              </Button>
            </Menu.Item>
          </Menu.Menu>
        </Menu>
      </Container>
    </Segment>
  </div>
);

NavigationMenu.propTypes = {
  transparentHeader: PropTypes.bool,
};

export default NavigationMenu;
