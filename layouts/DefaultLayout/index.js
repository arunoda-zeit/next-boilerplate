import React from 'react';
import PropTypes from 'prop-types';
import NavigationMenu from 'components/NavigationMenu';
import PageTitle from 'components/PageTitle';

const DefaultLayout = ({
  children, transparent, pageTitle, pageSubtitle,
}) => (
  <div>
    <NavigationMenu transparentHeader={transparent} />
    {pageTitle && <PageTitle title={pageTitle} subtitle={pageSubtitle} />}
    {children}
  </div>
);

DefaultLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  transparent: PropTypes.bool,
  pageTitle: PropTypes.string,
  pageSubtitle: PropTypes.string,
};

export default DefaultLayout;
