import React from 'react';
import { Grid, Divider } from 'semantic-ui-react';
import { compose } from 'redux';
import 'semantic-ui-css/semantic.min.css';

import DefaultLayout from 'layouts/DefaultLayout';
import Section from 'components/Section';
import ProductItem from 'components/ProductItem';
import withReduxSaga from 'utils/withReduxSaga';
import withIntl from 'utils/withIntl';

class Shop extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <DefaultLayout pageTitle="Shop" pageSubtitle="Start Buying your Favourite Theme" >
        <Divider section hidden />
        <Section>
          <Grid columns={4}>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
            <Grid.Column>
              <ProductItem name="Matthew" />
            </Grid.Column>
          </Grid>
        </Section>
        <Divider section hidden />
      </DefaultLayout>);
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

export default compose(
  withReduxSaga(mapStateToProps, mapDispatchToProps),
  withIntl,
)(Shop);
