import {
  LOAD_USERS,
  LOAD_USERS_SUCCESS,
  LOAD_USERS_FAILED,
} from './constants';

export const initialState = {
  users: [],
  fetching: false,
  error: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_USERS:
      return {
        ...state,
        ...{
          fetching: true,
          error: false,
        },
      };
    case LOAD_USERS_SUCCESS:
      return {
        ...state,
        ...{
          fetching: false,
          users: action.users,
        },
      };
    case LOAD_USERS_FAILED:
      return {
        ...state,
        ...{
          fetching: false,
          error: action.errors,
        },
      };
    default:
      return state;
  }
}

export default reducer;
