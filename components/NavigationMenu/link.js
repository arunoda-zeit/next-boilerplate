import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router';
import { Menu } from 'semantic-ui-react';

const Link = ({ name, href, router }) => {
  const handleMenuItemClick = (e) => {
    e.preventDefault();
    router.push(href);
  };

  return (<Menu.Item as="a" name={name} onClick={handleMenuItemClick} />);
};

Link.propTypes = {
  name: PropTypes.string,
  href: PropTypes.string,
  router: PropTypes.any,
};

export default withRouter(Link);
